package com.fragmadata.springbootstartapp.dao;

import com.fragmadata.springbootstartapp.bo.ConfigData;
import com.fragmadata.springbootstartapp.bo.Option;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository
public class TestDao {
    private JdbcTemplate jdbcTemplate;

    @Autowired
    public TestDao(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public static String GET_ALL_COUNTRY_CODE = "SELECT NAME, VALUE, TYPE FROM CONFIG_DATA WHERE TYPE = 'CountryCode' ";

    public static String SAVE_CONFIG_DATA = "INSERT INTO CONFIG_DATA (NAME, VALUE, TYPE) VALUES (?,?,?)";

    public static String UPDATE_CONFIG_DATA_BY_NAME = "UPDATE CONFIG_DATA SET VALUE = ?, TYPE = ? WHERE NAME = ? ";

    public static String SAVE_BATCH_CONFIG_DATA = "INSERT INTO CONFIG_DATA (NAME, VALUE, TYPE) VALUES (?,?,?)";

    public List<Option> getAllCountryCode() {

        return jdbcTemplate.query(GET_ALL_COUNTRY_CODE, new RowMapper<Option>() {
            @Override
            public Option mapRow(ResultSet resultSet, int rownumber) throws SQLException {
                Option option = new Option();

                option.setValue(resultSet.getString("NAME"));
                option.setId(resultSet.getString("VALUE"));

                return option;
            }
        });
    }

    // TODO : Save data

    public Boolean saveConfigData(ConfigData configData) {

        int saveCode = jdbcTemplate.update(SAVE_CONFIG_DATA, configData.getName(), configData.getValue(), configData.getType());

        if (saveCode == 1) {
            return true;
        } else {
            return false;
        }
    }

    // TODO : Update data
    public Boolean updateConfigDataByName(String name, ConfigData configData) {

        int updateCode = jdbcTemplate.update(UPDATE_CONFIG_DATA_BY_NAME, configData.getValue(), configData.getType(), name);

        if (updateCode == 1) {
            return true;
        } else {
            return false;
        }
    }

    // TODO : Batch insert
    public int[] batchInsertConfigData(List<ConfigData> configDatas) {

        return this.jdbcTemplate.batchUpdate(SAVE_BATCH_CONFIG_DATA,
                new BatchPreparedStatementSetter() {
                    public void setValues(PreparedStatement ps, int i) throws SQLException {
                        ps.setString(1, configDatas.get(i).getName());
                        ps.setString(2, configDatas.get(i).getValue());
                        ps.setString(3, configDatas.get(i).getType());
                    }

                    public int getBatchSize() {
                        return configDatas.size();
                    }
                });
    }


    // TODO : Get one Object

    /*public static String GET_USER_LOGIN_DETAILS_BY_TL_NO = "SELECT OTP_SEQ, TL_NO, EMAIL_OTP, MOBILE_OTP, STATUS, LOCKED_FROM, OTP_GEN_TS, OTP_EXP_TS FROM OTP_DETAILS WHERE TL_NO=? and STATUS = 'NOT_USED' ORDER BY OTP_GEN_TS desc";

    public UserDetailsBo getUserDetails(LoginDetails loginDetails) {
        UserDetailsBo userDetailsBo = new UserDetailsBo();

        jdbcTemplate.query(new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                PreparedStatement ps = connection.prepareStatement(GET_USER_LOGIN_DETAILS_BY_TL_NO);
                ps.setString(1, loginDetails.getTradeLicenseNumber());
                return ps;
            }
        }, new RowCallbackHandler() {
            @Override
            public void processRow(ResultSet resultSet) throws SQLException {
                userDetailsBo.setOtpSeq(resultSet.getLong("OTP_SEQ"));
                userDetailsBo.setTradeLicenseNumber(resultSet.getString("TL_NO"));
                userDetailsBo.setEmailOTP(resultSet.getString("EMAIL_OTP"));
                userDetailsBo.setMobileOTP(resultSet.getString("MOBILE_OTP"));
                userDetailsBo.setStatus(resultSet.getString("STATUS"));
                userDetailsBo.setLockedFrom(resultSet.getDate("LOCKED_FROM"));
                userDetailsBo.setOtpGenTs(resultSet.getDate("OTP_GEN_TS"));
                userDetailsBo.setOtpExpTs(resultSet.getTimestamp("OTP_EXP_TS"));
            }
        });

        return userDetailsBo;
    }*/


    // TODO : Get one value

    /*public static String GET_CUSTOMER_NAME_BY_TL_NO = "SELECT CLIENT_NAME FROM CASE_DETAILS WHERE TL_NO = ? AND CASE_STATUS != 'Completed' ";

    public String getCustomerNameByTlNo(String tlNo) {
        UserDetailsBo userDetailsBo = new UserDetailsBo();

        jdbcTemplate.query(new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                PreparedStatement ps = connection.prepareStatement(GET_CUSTOMER_NAME_BY_TL_NO);
                ps.setString(1, tlNo);
                return ps;
            }
        }, new RowCallbackHandler() {
            @Override
            public void processRow(ResultSet resultSet) throws SQLException {
                userDetailsBo.setCustomerName(resultSet.getString("CLIENT_NAME"));
            }
        });

        return userDetailsBo.getCustomerName();
    }*/
}
