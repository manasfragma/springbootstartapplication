package com.fragmadata.springbootstartapp.service;

import com.fragmadata.springbootstartapp.bo.ConfigData;
import com.fragmadata.springbootstartapp.bo.Option;
import com.fragmadata.springbootstartapp.dao.TestDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class TestService {
    private TestDao testDao;

    @Autowired
    public TestService(TestDao testDao) {
        this.testDao = testDao;
    }

    public List<Option> getAllTheConfigData() {
        return testDao.getAllCountryCode();
    }

    @Transactional
    public boolean saveOneConfigData(ConfigData configData) {
        return testDao.saveConfigData(configData);
    }

    @Transactional
    public boolean updateConfigDataByName(String name, ConfigData configData) {
        return testDao.updateConfigDataByName(name, configData);
    }

    @Transactional
    public int[] batchInsertConfigData(List<ConfigData> configDatas) {
        return testDao.batchInsertConfigData(configDatas);
    }
}