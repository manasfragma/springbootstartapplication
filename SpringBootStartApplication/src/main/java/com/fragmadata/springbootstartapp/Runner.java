package com.fragmadata.springbootstartapp;

import com.fragmadata.springbootstartapp.bo.ConfigData;
import com.fragmadata.springbootstartapp.bo.Option;
import com.fragmadata.springbootstartapp.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class Runner implements ApplicationRunner {

    private TestService testService;

    @Autowired
    public Runner(TestService testService) {
        this.testService = testService;
    }

    @Override
    public void run(ApplicationArguments applicationArguments) {

        System.out.println("::::::::APPLICATION EXECUTION START:::::::::::");

        // Get the config datas
        getConfigDatas();

        // Save Config Data
        saveConfigData();

        // Update Config Data
        updateConfigData();

        // Insert batch Config Data
        insertBatchConfigDatas();

        System.out.println("::::::::APPLICATION EXECUTION END::::::::::::::");

    }

    private void getConfigDatas() {
        List<Option> configDatas = null;

        configDatas = testService.getAllTheConfigData();

        int count = 1;
        for (Option option : configDatas) {
            System.out.println(":::: Record No : " + count + " ::::");
            System.out.println(":::: ID : " + option.getId() + " ::::");
            System.out.println(":::: Value : " + option.getValue() + " ::::");
            System.out.println("");
            ++count;
        }
    }

    private void saveConfigData() {
        boolean isSaved = false;
        ConfigData configData = new ConfigData();

        configData.setName("TestDataName");
        configData.setValue("TestDataValue");
        configData.setType("TestDataType");

        isSaved = testService.saveOneConfigData(configData);

        if (isSaved) {
            System.out.println("Data Saved Successfully");
        } else {
            System.out.println("Data not saved");
        }
    }

    private void updateConfigData() {
        boolean isSaved = false;
        ConfigData configData = new ConfigData();

        configData.setValue("TestDataValue1");
        configData.setType("TestDataType1");

        isSaved = testService.updateConfigDataByName("TestDataName", configData);

        if (isSaved) {
            System.out.println("Data Updated Successfully");
        } else {
            System.out.println("Data not Updated");
        }
    }


    private void insertBatchConfigDatas() {
        int[] output = null;
        ConfigData configData = null;
        List<ConfigData> configDatas = new ArrayList<>();

        for (int i = 1; i < 5; i++) {
            configData = new ConfigData();

            configData.setName("TestDataName" + i);
            configData.setValue("TestDataValue" + i);
            configData.setType("TestDataType" + i);

            configDatas.add(configData);
        }

        output = testService.batchInsertConfigData(configDatas);

        System.out.println("Data Saved :: " + output.length);

    }

}
